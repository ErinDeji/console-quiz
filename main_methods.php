<?php

class FuncMethods
{
    public $questions = [];
    public $answers = [];
    public $correctAnswers = [];
    public $answerEntered = "";
    public $correctAns = "";
    public $score = 0;
    public $indx = 0;
    public $ansIndex = 0;
    public $index = -1;
    public $highScore;

    ##       CREATE QUESTION
    public function createQuestion()
    {
        $i = readline("\e[0;35mType the question here:: \e[0m");
        $this->index++;
        array_push($this->questions, $i);
        for ($ind = 0; $ind < 3; $ind++) {
            $ans = readline("\e[0;36mchoose between this options \e[0m(a, b, c): ");
            $j = readline("\e[0;33mPlease enter the answer in words here: \e[0m");
            $this->answers = FuncMethods::arrayAssoc($this->answers, $this->index, $i, $ans, $j);
        }
        $correctAns = readline("\e[0;35mplease enter the correct answer here (a, b, c): \e[0m");
        array_push($this->correctAnswers, $correctAns);
    }
    public function arrayAssoc($array, $ind, $quest, $key, $value)
    {
        $array[$ind][$quest][$key] = $value;
        return $array;
    }


    ##      START QUIZ
    public function startQuiz()
    {

        foreach ($this->answers as $index => $question) {

            foreach ($question as $questions => $answers) {
                echo ($index + 1) . ".." . " " . $questions . "\n";
                foreach ($answers as $options => $values) {
                    echo $options . ".. " . $values . "\n";
                }
                $this->indx++;
                $ans = readline("\e[0;35mEnter the answer here:: \e[0m");
                $this->answerEntered = $ans;
                $correctAns = $this->correctAnswers[$this->ansIndex];
                if (strcmp($ans, $correctAns) == 0) {
                    $this->score++;
                } else {
                    echo "\e[0;32mThe correct answer is : \e[0m" . $correctAns . "\n";
                }
                $this->result();
                $this->ansIndex++;
            }
        }
    }

    ##      LIST ALL QUESTIONS
    public function listQuestion()
    {
        return $this->questions;
    }


    ##     CALCULATE TOTAL RESULT
    public function result()
    {

        if ($this->indx === count($this->questions)) {
            $scr = $this->score;
            $qt = count($this->questions);

            $totScore = $scr / $qt * 100;

            echo "\e[0;32mYou scored  : " . $totScore . " / 100  \e[0m\n";
            $this->score = 0;
            $this->indx = 0;
            $this->ansIndex = -1;
            $this->highScore = $totScore;
        }
    }


    ##     GET HIGH SCORE
    public function highScore()
    {
        if ($this->highScore >= $this->highScore) {
            return "\e[0;33mThe Current High Score is \e[0m" . $this->highScore . PHP_EOL;
        }
    }

    ##     REMOVE QUESTION

    public function removeQuestion($i)
    {
        $i = readline("\e[0;35mplease remove question here:: \e[0m");
        unset($this->questions[$i]);
        unset($this->answers[$i]);
        return $this->questions;
    }


    ##     QUIT APLLICATION
    public function Quit()
    {
        echo "\e[0;31mHave A Nice Day.!!!\e[0m\n";
        exit();
    }
}