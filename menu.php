<?php
require "main_methods.php";


function menu()
{
    echo "\n\e[0;41m             QUIZ APP            \e[0m\n";
    echo "\e[0;31m    ---------        ---------\n";
    echo "\e[0;31m         -------   ------\e[0m\n";
    echo "\e[0;32mPress 1: To Create A Questions\e[0m\n";
    echo "\e[0;32mPress 2: To List All Questions\e[0m\n";
    echo "\e[0;32mPress 3: To Start The Quiz\e[0m\n";
    echo "\e[0;32mPress 4: To Remove A Question\e[0m\n";
    echo "\e[0;32mPress 5: To Get High Scores\e[0m\n";
    echo "\e[0;31mPress 6: Quit\e[0m\n";
}
$ques = new FuncMethods;
while (true) {
    menu();
    $input = readline("\e[0;32mEnter input here: \e[0m");

    switch ($input) {
        case 1: {
                $ques->createQuestion();
                break;
            }
        case 2: {
                print_r($ques->listQuestion());
                print_r($ques->answers);
                break;
            }

        case 3: {
                $ques->startQuiz();
                break;
            }
        case 4: {
                $ques->removeQuestion($input);
                break;
            }
        case 5: {
                echo $ques->highScore();
                break;
            }
        case 6: {
                $ques->Quit();
                break;
            }

        default: {
                echo "\n\n\e[0;31mNo / wrong choice entered. Please provide a valid input.\e[0m\n\n";
            }
    }
}